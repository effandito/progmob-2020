package ukdw.com.progmob_2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.UTS.MainMatkulActivity;


public class HomeMenuActivity extends AppCompatActivity {
    ConstraintLayout currentLayout;
    String isLogin="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_menu);
        ImageButton btnMhs = (ImageButton)findViewById(R.id.btnMhs);
        ImageButton btnDosen = (ImageButton)findViewById(R.id.btnDosen);
        ImageButton btnMatkul = (ImageButton)findViewById(R.id.btnMatkul);
        Toolbar tb = (Toolbar) findViewById(R.id.tbMain);
        setSupportActionBar(tb);

        btnMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeMenuActivity.this, MahasiswaActivity.class);
                startActivity(intent);
            }
        });

        btnDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeMenuActivity.this, DosenActivity.class);
                startActivity(intent);
            }
        });

        btnMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeMenuActivity.this, MainMatkulActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item){
        currentLayout = (ConstraintLayout)findViewById(R.id.ConstraintLayout);
        SharedPreferences pref = HomeMenuActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        switch (item.getItemId()){
            case R.id.tbLogout:
                isLogin = pref.getString("isLogin","0");
                editor.putString("isLogin","0");
                editor.commit();
                Intent intent = new Intent(HomeMenuActivity.this, LoginMainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
