package ukdw.com.progmob_2020.UTS;

import androidx.appcompat.app.AppCompatActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.CRUD.MahasiswaUpdateActivity;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class UtsMhsUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uts_mhs_update);

        final EditText edEditNimCari = (EditText)findViewById(R.id.txtNimCariUts);
        final EditText edEditNamaBaruMhsUts = (EditText)findViewById(R.id.txtNamaBaruMhsUts);
        final EditText edEditNimBaruMhsUts = (EditText)findViewById(R.id.txtNimBaruMhsUts);
        final EditText edEditAlamatBaruMhsUts = (EditText)findViewById(R.id.txtAlamatBaruMhsUts);
        final EditText edEditEmailBaruMhsUts = (EditText)findViewById(R.id.txtEmailBaruMhsUts);
        Button btnUpdateMhsUts = (Button) findViewById(R.id.btnUpdateMhsUts);
        pd = new ProgressDialog(UtsMhsUpdateActivity.this);

        Intent data = getIntent();
        if(data!=null){
            edEditNimCari.setText(data.getStringExtra("nim"));
            edEditNimBaruMhsUts.setText(data.getStringExtra("nim"));
            edEditNamaBaruMhsUts.setText(data.getStringExtra("nama"));
            edEditAlamatBaruMhsUts.setText(data.getStringExtra("alamat"));
            edEditEmailBaruMhsUts.setText(data.getStringExtra("email"));}

        btnUpdateMhsUts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu...");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> up = service.delete_mhs(
                        edEditNimCari.getText().toString(),
                        "72180262"
                );

                up.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Toast.makeText(UtsMhsUpdateActivity.this,"Berhasil disimpan",Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UtsMhsUpdateActivity.this,"Error",Toast.LENGTH_LONG);
                    }
                });
                Call<DefaultResult> addMhs = service.add_mhs(
                        edEditNamaBaruMhsUts.getText().toString(),
                        edEditNimBaruMhsUts.getText().toString(),
                        edEditAlamatBaruMhsUts.getText().toString(),
                        edEditEmailBaruMhsUts.getText().toString(),
                        "Kosongkan",
                        "72180262"
                );
                addMhs.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UtsMhsUpdateActivity.this,"Berhasil disimpan",Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UtsMhsUpdateActivity.this,"Error",Toast.LENGTH_LONG);

                    }
                });
            }
        });

    }
}