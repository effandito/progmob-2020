package ukdw.com.progmob_2020.UTS;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Adapter.MatakuliahAdapterUts;
import ukdw.com.progmob_2020.Model.Matakuliah;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class MatkulUtsActivity extends AppCompatActivity {
    RecyclerView rvMatkulUts;
    Matakuliah matkulAdapterUts;
    ProgressDialog pd;
    List<Matakuliah> matkulList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_uts);
        rvMatkulUts = (RecyclerView)findViewById(R.id.rvMatkul);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon menunggu");
        pd.show();

        androidx.appcompat.widget.Toolbar tbMatkulUts = (androidx.appcompat.widget.Toolbar) findViewById(R.id.tbMatkul);
        setSupportActionBar(tbMatkulUts);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Matakuliah>> call = service.getMatkul("72180262");

        call.enqueue(new Callback<List<Matakuliah>>() {
            @Override
            public void onResponse(Call<List<Matakuliah>> call, Response<List<Matakuliah>> response) {
                pd.dismiss();
                matkulList = response.body();
                matkulAdapterUts= new MatakuliahAdapterUts(matkulList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MatkulUtsActivity.this);
                rvMatkulUts.setLayoutManager(layoutManager);
                rvMatkulUts.setAdapter(matkulAdapterUts);
            }

            @Override
            public void onFailure(Call<List<Matakuliah>> call, Throwable t) {
                Toast.makeText(MatkulUtsActivity.this,"Error",Toast.LENGTH_LONG);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add,menu);
        return  super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.tbAdd:
                Intent intent = new Intent(MatkulUtsActivity.this,MatkulAddActivity.class);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}

