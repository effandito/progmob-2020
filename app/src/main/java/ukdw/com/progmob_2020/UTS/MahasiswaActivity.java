package ukdw.com.progmob_2020.UTS;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Adapter.MahasiswaAdapterUts;
import ukdw.com.progmob_2020.Adapter.MahasiswaCRUDRecyclerAdapter;
import ukdw.com.progmob_2020.CRUD.MahasiswaGetAllActiviity;
import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class MahasiswaActivity extends AppCompatActivity {
    RecyclerView rvMhs;
    MahasiswaAdapterUts mhsAdapterUts;
    ProgressDialog pd;
    List<Mahasiswa> mahasiswaList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa);
        rvMhs = (RecyclerView)findViewById(R.id.rvActivityMhs);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Menunggu");
        pd.show();
        androidx.appcompat.widget.Toolbar tbMhsUts = (androidx.appcompat.widget.Toolbar)findViewById(R.id.tbMhsUts);
        setSupportActionBar(tbMhsUts);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        //mengambil data
        Call<List<Mahasiswa>> call = service.getMahasiswa("72180262");

        call.enqueue(new Callback<List<Mahasiswa>>() {
            @Override
            public void onResponse(Call<List<Mahasiswa>> call, Response<List<Mahasiswa>> response) {
                pd.dismiss();
                mahasiswaList = response.body();
                mhsAdapterUts = new MahasiswaAdapterUts(mahasiswaList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MahasiswaActivity.this);
                rvMhs.setLayoutManager(layoutManager);
                rvMhs.setAdapter(mhsAdapterUts);
            }

            @Override
            public void onFailure(Call<List<Mahasiswa>> call, Throwable t) {
                Toast.makeText(MahasiswaActivity.this,"Error", Toast.LENGTH_LONG);
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.tbAdd:
                Intent intent = new Intent(MahasiswaActivity.this, UtsMhsAddActivity.class);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}