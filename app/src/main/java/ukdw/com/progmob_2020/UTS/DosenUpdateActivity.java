package ukdw.com.progmob_2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class DosenUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_update);

        final EditText edNidn = (EditText) findViewById(R.id.txtEditNidn);
        final EditText edUpdateNamaDsn = (EditText) findViewById(R.id.txtUpdateNamaDsn);
        final EditText edUpdateNidn = (EditText) findViewById(R.id.txtUpdateNidnDsn);
        final EditText edUpdateAlamatDsn = (EditText) findViewById(R.id.txtUpdateAlamatDsn);
        final EditText edUpdateEmailDsn = (EditText) findViewById(R.id.txtUpdateEmailDsn);
        final EditText edUpdateGelarDsn = (EditText) findViewById(R.id.txtUpdateGelarDsn);
        Button btnUpdateDsn = (Button) findViewById(R.id.btnUpdateDsn);
        pd = new ProgressDialog(DosenUpdateActivity.this);

        Intent data = getIntent();
        if (data != null) {
            edNidn.setText(data.getStringExtra("nidn"));
            edUpdateNamaDsn.setText(data.getStringExtra("nama"));
            edUpdateNidn.setText(data.getStringExtra("nidn_cari"));
            edUpdateAlamatDsn.setText(data.getStringExtra("alamat"));
            edUpdateEmailDsn.setText(data.getStringExtra("email"));
            edUpdateGelarDsn.setText(data.getStringExtra("gelar"));

            btnUpdateDsn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pd.setTitle("Please Wait");
                    pd.show();

                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                    Call<DefaultResult> upd = service.delete_dsn(
                            edNidn.getText().toString(),
                            "72180262"
                    );
                    upd.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            Toast.makeText(DosenUpdateActivity.this, "Berhasil Disimpan", Toast.LENGTH_LONG);
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(DosenUpdateActivity.this, "Error", Toast.LENGTH_LONG);

                        }
                    });
                    Call<DefaultResult> adddsn = service.add_dsn(
                            edUpdateNamaDsn.getText().toString(),
                            edUpdateNidn.getText().toString(),
                            edUpdateAlamatDsn.getText().toString(),
                            edUpdateEmailDsn.getText().toString(),
                            edUpdateGelarDsn.getText().toString(),
                            "kosongkan",
                            "72180262"
                    );
                    adddsn.enqueue(new Callback<DefaultResult>() {
                        @Override
                        public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                            pd.dismiss();
                            Toast.makeText(DosenUpdateActivity.this, "Berhasil Disimpan", Toast.LENGTH_LONG);
                        }

                        @Override
                        public void onFailure(Call<DefaultResult> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(DosenUpdateActivity.this, "Error", Toast.LENGTH_LONG);
                        }
                    });
                }
            });
        }
    }
}