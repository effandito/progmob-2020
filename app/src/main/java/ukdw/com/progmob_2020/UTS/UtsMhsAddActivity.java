package ukdw.com.progmob_2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.CRUD.MahasiswaAddActivity;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class UtsMhsAddActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uts_mhs_add);

        final EditText edAddNama = (EditText)findViewById(R.id.txtAddNamaMhsUts);
        final EditText edAddNim = (EditText)findViewById(R.id.txtAddNimMhsUts);
        final EditText edAddAlamat = (EditText)findViewById(R.id.txtAddAlamatMhsUts);
        final EditText edAddEmail = (EditText)findViewById(R.id.txtAddEmailMhsUts);
        Button btnAddSimpan = (Button)findViewById(R.id.btnAddSimpanMhsUts);
        pd = new ProgressDialog(UtsMhsAddActivity.this);

        btnAddSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Please Waiting");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_mhs(
                        edAddNama.getText().toString(),
                        edAddNim.getText().toString(),
                        edAddAlamat.getText().toString(),
                        edAddEmail.getText().toString(),
                        "Kosong",
                        "72180262"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UtsMhsAddActivity.this, "Data Berhasil Disimpan", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UtsMhsAddActivity.this, "GAGAL", Toast.LENGTH_LONG).show();

                    }
                });
            }
        });
    }
}