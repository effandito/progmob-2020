package ukdw.com.progmob_2020.UTS;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;
public class MatkulUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_update);
        final EditText edKodeCari = (EditText)findViewById(R.id.edKodeCa);
        final EditText edNamaMatkul = (EditText)findViewById(R.id.edNamaMkl);
        final EditText edKode = (EditText)findViewById(R.id.edKodeB);
        final EditText edHari = (EditText)findViewById(R.id.edHariMkl);
        final EditText edSesi = (EditText)findViewById(R.id.edSesiMkl);
        final EditText edSks = (EditText)findViewById(R.id.edSksMkl);
        Button btnUbah = (Button)findViewById(R.id.btnUpdateMkl);
        pd = new ProgressDialog(MatkulUpdateActivity.this);

        btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> upd = service.update_matkul(
                        edKodeCari.getText().toString(),
                        edNamaMatkul.getText().toString(),
                        "72180262",
                        edKode.getText().toString(),
                        Integer.parseInt(edHari.getText().toString()),
                        Integer.parseInt(edSesi.getText().toString()),
                        Integer.parseInt(edSks.getText().toString())
                );

                upd.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Toast.makeText(MatkulUpdateActivity.this,"Berhasil disimpan",Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this,"Error",Toast.LENGTH_LONG);
                    }
                });
            }
        });
    }
}