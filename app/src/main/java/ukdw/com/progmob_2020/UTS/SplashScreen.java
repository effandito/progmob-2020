package ukdw.com.progmob_2020.UTS;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

public class SplashScreen {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        startActivity(new Intent(this, LoginActivity.class));
    }
}
