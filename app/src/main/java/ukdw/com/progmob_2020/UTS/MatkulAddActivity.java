package ukdw.com.progmob_2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class MatkulAddActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_add);
        final EditText edNamaMatkul = (EditText) findViewById(R.id.edNamaMatkul);
        final EditText edKode = (EditText)findViewById(R.id.edKode);
        final EditText edHari = (EditText)findViewById(R.id.edHari);
        final EditText edSesi = (EditText)findViewById(R.id.edSesi);
        final EditText edSks = (EditText)findViewById(R.id.edSks);
        Button btnSimpan = (Button)findViewById(R.id.btnSimpanMatkul);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Tunggu Sebentar");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_matkul(
                        edNamaMatkul.getText().toString(),
                        "72180262",
                        edKode.getText().toString(),
                        Integer.parseInt(edHari.getText().toString()),
                        Integer.parseInt(edSesi.getText().toString()),
                        Integer.parseInt(edSks.getText().toString())
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MatkulAddActivity.this, "Data Berhasil Ditambahkan !", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulAddActivity.this, "Gagal Menambahkan Data! !", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}
