package ukdw.com.progmob_2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import ukdw.com.progmob_2020.R;

public class DosenViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_view);
        TextView tvNidn = (TextView) findViewById(R.id.txtVmNidnDsnUts);
        TextView tvNamaDsn = (TextView) findViewById(R.id.txtVmNamaDsnUts);
        TextView tvAlamatDsn = (TextView) findViewById(R.id.txtVmAlamatDsnUts);
        TextView tvEmailDsn = (TextView) findViewById(R.id.txtVmEmailDsnUts);
        TextView tvGelar = (TextView) findViewById(R.id.txtVmGelarDsnUts);

        Intent data = getIntent();
        if (data != null){
            tvNidn.setText(data.getStringExtra("nidn"));
            tvNamaDsn.setText(data.getStringExtra("nama"));
            tvAlamatDsn.setText(data.getStringExtra("alamat"));
            tvEmailDsn.setText(data.getStringExtra("email"));
            tvGelar.setText(data.getStringExtra("gelar"));

        }
    }
}