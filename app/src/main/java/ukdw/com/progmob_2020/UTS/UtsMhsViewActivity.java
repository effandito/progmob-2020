package ukdw.com.progmob_2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.content.Intent;
import android.widget.TextView;
import ukdw.com.progmob_2020.R;

public class UtsMhsViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uts_mhs_view);
        TextView tvNim = (TextView)findViewById(R.id.txtVwNimMhsUts);
        TextView tvNama = (TextView)findViewById(R.id.txtVwNamaMhsUts);
        TextView tvAlamat = (TextView)findViewById(R.id.txtVwAlamatMhsUts);
        TextView tvEmail = (TextView)findViewById(R.id.txtVwEmailMhsUts);

        Intent data = getIntent();
        if(data !=null){
            tvNim.setText(data.getStringExtra("nim"));
            tvNama.setText(data.getStringExtra("nama"));
            tvAlamat.setText(data.getStringExtra("alamat"));
            tvEmail.setText(data.getStringExtra("email"));
        }
    }
}
