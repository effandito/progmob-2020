package ukdw.com.progmob_2020.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Model.Dosen;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.UTS.DosenUpdateActivity;
import ukdw.com.progmob_2020.UTS.DosenViewActivity;

public class DosenAdapterUts {
    private Context context;
    private List<Dosen> dosenList;
    ProgressDialog pd;

    public DosenAdapterUts(Context context) {
        this.context = context;
        dosenList = new ArrayList<>();
    }

    public DosenAdapterUts(List<Dosen> dosenList)
    {
        this.dosenList = dosenList;
    }

    public List<Dosen> getDosenList) {
        return dosenList;
    }

    public void setDosenList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_dosen,parent,false);
        return new ViewHolder(v,parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull DosenAdapterUts.ViewHolder holder, int position) {
        Dosen d = dosenList.get(position);
        holder.tvNamaDsn.setText(d.getNama());
        holder.tvEmailDsn.setText(d.getEmail());
        holder.dsn = d;
        pd = new ProgressDialog(holder.context2);
        holder.btnDsnUpDelViwUts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dosen d = dosenList.get(position);
                PopupMenu popupMenu = new
                        PopupMenu(holder.context2,holder.btnDsnUpDelViwUts);
                popupMenu.inflate(R.menu.menu_edit);
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                        switch (menuItem.getItemId()){
                            case R.id.tbDelete:
                                pd.setTitle("Deleted on Process");
                                pd.show();
                                Call<DefaultResult> del=service.delete_dsn(d.getNidn().toString(),"72180262");
                                del.enqueue(new Callback<DefaultResult>() {
                                    @Override
                                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                                        pd.dismiss();
                                        Toast.makeText(holder.context2,"Berhasil dihapus",Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                                        pd.dismiss();
                                        Toast.makeText(holder.context2,"Gagal dihapus",Toast.LENGTH_LONG).show();
                                    }
                                });
                                break;
                            case R.id.tbView:
                                Intent dsnVw = new Intent(holder.context2, DosenViewActivity.class);
                                dsnVw.putExtra("nidn",d.getNidn());
                                dsnVw.putExtra("nama",d.getNama());
                                dsnVw.putExtra("alamat",d.getAlamat());
                                dsnVw.putExtra("email",d.getEmail());
                                dsnVw.putExtra("gelar",d.getGelar());
                                holder.context2.startActivity(dsnVw);
                                break;
                            case R.id.tbUpdate:
                                Intent dsnUpd = new Intent(holder.context2, DosenUpdateActivity.class);
                                dsnUpd.putExtra("nidn_cari",d.getNidn_cari());
                                dsnUpd.putExtra("nama",d.getNama());
                                dsnUpd.putExtra("nidn", d.getNidn());
                                dsnUpd.putExtra("alamat", d.getAlamat());
                                dsnUpd.putExtra("email", d.getEmail());
                                dsnUpd.putExtra("gelar", d.getGelar());
                                holder.context2.startActivity(dsnUpd);
                        }
                        return false;
                    }
                });
            }
        });
    }
   @Override
    public int getItemCount() {
        return dosenList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNamaDsn, tvEmailDsn, tvNoTelp;
        private ImageView btnDsnUpDelViwUts;
        Context context2;
        Dosen dsn;
        public ViewHolder(@NonNull View itemView, Context context) {
            super(itemView);
            tvNamaDsn = itemView.findViewById(R.id.txtNamaDsn);
            tvEmailDsn = itemView.findViewById(R.id.txtEmailDsn);
            btnDsnUpDelViwUts = itemView.findViewById(R.id.btnDsnVwDelUpUts);
            context2 = context;
        }
    }
}

