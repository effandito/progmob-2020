package ukdw.com.progmob_2020.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.Model.Matakuliah;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.DosenViewActivity;
import ukdw.com.progmob_2020.UTS.DosenUpdateActivity;
import ukdw.com.progmob_2020.UTS.MatkulUpdateActivity;
import ukdw.com.progmob_2020.UTS.MatkulViewActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import java.util.List;

public class MatakuliahAdapterUts extends RecyclerView.Adapter<MatakuliahAdapterUts.ViewHolder> {

    private Context context;
    private List<Matakuliah>matkulList;
    ProgressDialog pd;

    public MatakuliahAdapterUts(Context context){
        this.context = context;
        matkulList = new ArrayList<>();
    }

    public MatakuliahAdapterUts(List<Matakuliah>matkulList){
        this.matkulList = matkulList;
    }

    public List<Matakuliah>getMatkulList(){
        return matkulList;
    }

    public void setMatkulList(List<Matakuliah>matkulList){
        this.matkulList = matkulList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_matkul,parent,false);
        return  new MatakuliahAdapterUts.ViewHolder(v,parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Matakuliah mk = matkulList.get(position);
        holder.tvKodeMatkul.setText(mk.getKode());
        holder.tvNamaMatkul.setText(mk.getNama());
        holder.mtk = mk;
        pd = new ProgressDialog(holder.context3);
        holder.btnMatkulVUD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new
                        PopupMenu(holder.context3,holder.btnMatkulVUD);
                popupMenu.inflate(R.menu.menu_edit);
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                        switch (item.getItemId()){
                            case R.id.tbDel:
                                pd.setTitle("Deleting");
                                pd.show();
                                Call<DefaultResult> del = service.delete_matkul(mk.getKode().toString(),"72180262");
                                del.enqueue(new Callback<DefaultResult>() {
                                    @Override
                                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                                        pd.dismiss();
                                        Toast.makeText(holder.context3,"Deleted",Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                                        pd.dismiss();
                                        Toast.makeText(holder.context3,"Failed",Toast.LENGTH_LONG).show();
                                    }
                                });
                                break;
                            case R.id.tbView:
                                Intent matkulVw = new Intent(holder.context3, MatkulViewActivity.class);
                                matkulVw.putExtra("nidn",mk.getKode());
                                matkulVw.putExtra("nama",mk.getNama());
                                matkulVw.putExtra("hari",mk.getHari());
                                matkulVw.putExtra("sesi",mk.getSesi());
                                matkulVw.putExtra("sks",mk.getSks());
                                holder.context3.startActivity(matkulVw);
                                break;
                            case R.id.tbUpdate:
                                Intent matkulUp = new Intent(holder.context3, MatkulUpdateActivity.class);
                                matkulUp.putExtra("nama",mk.getNama());
                                matkulUp.putExtra("kode",mk.getKode());
                                matkulUp.putExtra("hari",mk.getHari());
                                matkulUp.putExtra("sesi",mk.getSesi());
                                matkulUp.putExtra("sks",mk.getSks());
                                holder.context3.startActivity(matkulUp);
                        }
                        return false;
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return matkulList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvKodeMatkul, tvNamaMatkul;
        private ImageView btnMatkulVUD;
        Context context3;
        Matakuliah mtk;

        public ViewHolder(@NonNull View itemView, Context context){
            super(itemView);
            tvKodeMatkul = itemView.findViewById(R.id.txtVwKodeMktUts);
            tvNamaMatkul = itemView.findViewById(R.id.txtVwNamaMklUts);
            btnMatkulVUD = itemView.findViewById(R.id.btnUpdateMkl);
            context3 = context;
        }
    }
}

